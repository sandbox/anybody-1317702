<?php

/**
 * @file
 * Menu callbacks and associated methods for Ajax load example.
 */

/**
 * Menu callback: present a page with a link.
 */
function ajax_menu_load_page() {
  // If this is an AJAX request, return the AJAX result.
  if (isset($_REQUEST['selected_menu']) and isset($_REQUEST['mlid'])) {
    ajax_menu_load_callback($_REQUEST['selected_menu'], $_REQUEST['mlid']);
  }
  else {
    ajax_menu_load_callback();
  }
}

function get_selected_menu_tree($selected_menu, $mlid) {
  $menu_tree = menu_tree_all_data($selected_menu);
  // Find selected MLID.
  foreach ($menu_tree as $current_tree) {
    if ($mlid !== NULL and isset($current_tree['link']['mlid']) and $current_tree['link']['mlid'] == $mlid) {
      if (!empty($current_tree['below'])) {
        $selected_menu_tree = $current_tree['below'];

        // Remove further Submenus.
        foreach ($selected_menu_tree as $key => $subtree) {
          $selected_menu_tree[$key]['below'] = FALSE;
        }
        break;
      }
      else {
        $selected_menu_tree = NULL;
      }
    }
    else {
      $selected_menu_tree = NULL;
    }
  }

  // Remove below.
  return $selected_menu_tree;
}

/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function ajax_menu_load_callback($selected_menu, $mlid = NULL) {
  $selected_menu_tree = get_selected_menu_tree($selected_menu, $mlid);

  if (! empty($selected_menu_tree)) {
    $content = menu_tree_output($selected_menu_tree);
  }
  else {
    $content = NULL;
  }
  $result = array(
    'content' => $content, // Put the Javascript callback you will use here.
    // You can if you wish leave out this line and instead
    // call your callback directly in your Javascript. See
    // comments in ajax_load_example.js.
    '__callbacks' => array(
      'Drupal.ajax_menu_load.ajax_menu_callback'
    )
  );
  // Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  // add its data and register its Javascript callback.
  // The second argument is the data to be altered.
  // The third argument is a unique identifier for this AJAX data operation.
  // The fourth and optional argument is the original data that was the subject
  // of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'ajax_menu_load');
  drupal_json($result);
}