(function ($) {
	var ajax_load_cache = new Array();
	var targetUrl = '/ajax-menu-load';
	Drupal.ajax_menu_load = Drupal.ajax_menu_load || {};
	//Additional HTML to reach equal output
	var prepend_html = '<div class="block block-menu_block region-odd even region-count-1 count-2" id="block-menu_block-2"><div class="block-inner"><div class="content"><div class="menu-block-2 menu-name-primary-links parent-mlid-0 menu-level-2">';
	var append_html = '</div></div></div></div>';
	
	/**
	 * Ajax load example callback. 
	 */
	Drupal.ajax_menu_load.ajax_menu_callback = function (target, response) {
		if(response.content != null){
			$(target).html(prepend_html+response.content+append_html);
			if($(target).is(":hidden") || $(target).html()==''){
				 $(target).slideDown("slow");
			}
			Drupal.attachBehaviors(target); 
		} else {
			$(target).slideUp("normal");
		}
	};
	
	Drupal.ajax_menu_load.loadMenu = function (target, selected_menu_name, selected_menu_mlid) {		
		if(typeof ajax_load_cache[selected_menu_name+'.'+selected_menu_mlid]!='undefined'){
			Drupal.ajax_menu_load.ajax_menu_callback(target, ajax_load_cache[selected_menu_name+'.'+selected_menu_mlid]);
		} else {
			$.ajax({
		        // Either GET or POST will work.
		        type: 'GET',
		        data: {'ajax_menu_load':'1',
		        	   'selected_menu':selected_menu_name,
		        	   'mlid':selected_menu_mlid},
		        // Need to specify JSON data.
		        dataType: 'json',
		        url: targetUrl,
		        beforeSend: function(XMLHttpRequest){
		        	$(target).html('<img src="/sites/all/themes/jptf/images/menu_ajax-loader.gif" alt="Loading..." class="ajax-loader" id="menu_ajax-loader" />');
		        },
		        success: function(response){     
		        	ajax_load_cache[selected_menu_name+'.'+selected_menu_mlid] = response;
		            Drupal.ajax_menu_load.ajax_menu_callback(target, response);
		        }
		     });
	 	}   
	};
})(jQuery);